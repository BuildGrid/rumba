image: docker:stable
services:
  - docker:dind

stages:
  - build
  - test
  - release

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

# Check C++ code formatting using clang-format
# Since the GitLab CI API doesn't currently provide an MR Commit SHA so that we can
# run all the files modified in the current MR (Single Commit is doable) we just
# run clang-format for the diff between "empty tree magic SHA" and the current commit
# on all the C++ files (by extension: c,cc,cpp,cxx,h)
# Discussion on the "empty tree magic SHA": https://stackoverflow.com/questions/9765453/
check_formatting:
    image: ubuntu:bionic
    stage: build
    before_script:
        - apt-get update && apt-get install -y clang-format-6.0 git-core
    script:
        - echo `which clang-format-6.0`
        - ln -s `which clang-format-6.0` /usr/bin/clang-format
        - cd "$CI_PROJECT_DIR"
        - export CLANG_FORMAT_SINCE_COMMIT="4b825dc642cb6eb9a060e54bf8d69288fbee4904"
        - linter_errors=$(git-clang-format-6.0 --commit "$CLANG_FORMAT_SINCE_COMMIT" -q --diff --style file --extensions c,cc,cpp,cxx,h | grep -v --color=never "no modified files to format" || true)
        - echo "$linter_errors"
        - if [[ ! -z "$linter_errors" ]]; then echo "Detected formatting issues; please fix"; exit 1; else echo "Formatting is correct"; exit 0; fi

build:
  stage: build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull --build-arg BUILD_TESTS=ON --build-arg EXTRA_CMAKE_FLAGS="-DCMAKE_CXX_FLAGS=-O1 -D_FORTIFY_SOURCE=2" -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE

tests:
  stage: test
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CONTAINER_TEST_IMAGE
    - docker network create rumbad-tests
    - docker run -w /rumba/build --network rumbad-tests $CONTAINER_TEST_IMAGE sh -c 'ctest --verbose'

integration-test:
  stage: test
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run -w /rumba/integration $CONTAINER_TEST_IMAGE ./test-rumbad-output.sh

e2e:
  image: docker:dind
  services: []
  stage: test
  dependencies: []
  before_script:
    - export DOCKER_SOCKET="/var/run/docker.sock"
    - export DOCKER_HOST="unix://$DOCKER_SOCKET"
    - export E2E_IMAGE="registry.gitlab.com/buildgrid/buildbox/buildbox-e2e/new-e2e"
    - DOCKER_TLS_CERTDIR=/certs dockerd-entrypoint.sh &
    - sleep 3
    - docker version
    - docker pull "$E2E_IMAGE"
  script:
    - docker run -v "$DOCKER_SOCKET":"$DOCKER_SOCKET" -v $(pwd):/buildbox-e2e/src/rumba "$E2E_IMAGE"

release-image:
  stage: release
  script:
    # Release contains a build with the default compile flags.
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CONTAINER_RELEASE_IMAGE .
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - master

trigger_e2e_rebuild:
    image: ubuntu:bionic
    stage: release
    variables:
        GIT_STRATEGY: none
    script:
        - apt-get update && apt-get install -y curl
        - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.com/api/v4/projects/buildgrid%2Fbuildbox%2Fbuildbox-e2e/trigger/pipeline
    only:
        - master
