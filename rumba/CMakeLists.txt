set_source_files_properties(${PROTO_GENERATED_SRCS} PROPERTIES COMPILE_FLAGS "-Wno-all -Wno-error -Wno-extra -Wno-conversion" GENERATED 1)

add_executable(rumba
    rumba.m.cpp
    rumba_datautils.cpp
    ${PROTO_GENERATED_SRCS}
    # add more source files to the rumba build here
)

add_dependencies(rumba generate_protobufs)

target_include_directories(rumba PUBLIC ./ ${PROTO_GEN_DIR})
target_link_libraries(rumba PUBLIC
    ${_EXTRA_LDD_FLAGS}
    Buildbox::buildboxcommon
    ${OPENSSL_TARGET}
    ${GRPC_TARGET}
    ${PROTOBUF_TARGET}
    recc::remoteexecution
    # add more direct dependencies to the rumba build here
)

install(
    TARGETS rumba
    DESTINATION bin
    COMPONENT rumba
)
